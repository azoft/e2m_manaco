package com.e2m.manaco.restapiserver.service;

import com.e2m.manaco.commonmodule.model.Export;
import com.e2m.manaco.hibernatemodule.service.HibernateService;

public interface ExportService extends HibernateService<Export, Long> {
}
