package com.e2m.manaco.restapiserver.dto;

public class ResponseData<T> extends RequestData<T> {

    private Integer code = 200;

    private String error;

    private String message;

    public ResponseData() {
        super();
    }

    public ResponseData(T data) {
        super(data);
    }

    public ResponseData(Integer code, String error, String message) {
        this.code = code;
        this.error = error;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ResponseData{"
                + "code=" + code
                + ", error='" + error + '\''
                + ", message='" + message + '\''
                + ", data=" + getData()
                + '}';
    }
}
