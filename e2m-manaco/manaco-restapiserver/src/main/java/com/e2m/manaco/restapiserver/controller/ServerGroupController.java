package com.e2m.manaco.restapiserver.controller;

import static com.e2m.manaco.restapiserver.controller.ServerGroupController.URL_V1_SERVER_GROUP;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.e2m.manaco.commonmodule.model.ServerGroup;
import com.e2m.manaco.hibernatemodule.dto.ListResult;
import com.e2m.manaco.restapiserver.service.ServerGroupService;
import com.e2m.manaco.restapiserver.util.RestApiError;

@RestController
@RequestMapping(value = URL_V1_SERVER_GROUP)
public class ServerGroupController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(ServerGroupController.class);

    static final String URL_V1_SERVER_GROUP = URL_V1 + "/servergroup";
    static final String VAR_SERVER_GROUP_ID = "/{serverGroupId}";

    @Autowired
    private ServerGroupService serverGroupService;

    @GetMapping
    public ResponseEntity findServerGroups() {
        ListResult<ServerGroup> serverGroups = serverGroupService.findAll();
        log.info("findServerGroups, result: {}", serverGroups);
        return createResponseEntity(serverGroups);
    }

    @GetMapping(VAR_SERVER_GROUP_ID)
    public ResponseEntity getServerGroup(@PathVariable Long serverGroupId) {
        ServerGroup serverGroup = serverGroupService.get(serverGroupId);
        log.info("getServerGroup serverGroupId: {}, result: {}", serverGroupId, serverGroup);
        if (serverGroup == null) {
            return createResponseEntity(RestApiError.SERVER_GROUP_NOT_FOUND, serverGroupId);
        }
        return createResponseEntity(serverGroup);
    }
}
