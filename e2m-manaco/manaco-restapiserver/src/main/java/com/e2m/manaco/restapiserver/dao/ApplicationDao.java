package com.e2m.manaco.restapiserver.dao;

import com.e2m.manaco.commonmodule.model.Application;
import com.e2m.manaco.hibernatemodule.dao.HibernateDao;

public interface ApplicationDao extends HibernateDao<Application, Long> {
}
