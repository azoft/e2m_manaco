package com.e2m.manaco.restapiserver.dao;

import com.e2m.manaco.commonmodule.model.Server;
import com.e2m.manaco.hibernatemodule.dao.HibernateDao;

public interface ServerDao extends HibernateDao<Server, Long> {
}
