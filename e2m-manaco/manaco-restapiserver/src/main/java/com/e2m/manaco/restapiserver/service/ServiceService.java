package com.e2m.manaco.restapiserver.service;

import com.e2m.manaco.commonmodule.model.Service;
import com.e2m.manaco.hibernatemodule.service.HibernateService;

public interface ServiceService extends HibernateService<Service, Long> {
}
