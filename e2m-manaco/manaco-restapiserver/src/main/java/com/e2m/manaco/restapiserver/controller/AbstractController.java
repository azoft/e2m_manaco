package com.e2m.manaco.restapiserver.controller;

import org.springframework.http.ResponseEntity;

import com.e2m.manaco.restapiserver.dto.ResponseData;
import com.e2m.manaco.restapiserver.util.RestApiError;

public abstract class AbstractController {

    static final String URL_V1 = "/v1";

    <T> ResponseEntity createResponseEntity(T object) {
        ResponseData<T> data = new ResponseData<>(object);
        return ResponseEntity.ok(data);
    }

    ResponseEntity createResponseEntity(RestApiError restApiError, Object... args) {
        String message = String.format(restApiError.getFormat(), args);
        ResponseData data = new ResponseData<>(restApiError.getStatus().value(), restApiError.name(), message);
        return new ResponseEntity<>(data, restApiError.getStatus());
    }
}
