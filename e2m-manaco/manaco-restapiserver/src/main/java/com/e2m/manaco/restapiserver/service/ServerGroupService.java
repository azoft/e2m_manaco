package com.e2m.manaco.restapiserver.service;

import com.e2m.manaco.commonmodule.model.ServerGroup;
import com.e2m.manaco.hibernatemodule.service.HibernateService;

public interface ServerGroupService extends HibernateService<ServerGroup, Long> {
}
