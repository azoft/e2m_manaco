package com.e2m.manaco.restapiserver.dao.impl;

import org.springframework.stereotype.Repository;

import com.e2m.manaco.commonmodule.model.Application;
import com.e2m.manaco.hibernatemodule.dao.impl.AbstractHibernateDao;
import com.e2m.manaco.restapiserver.dao.ApplicationDao;

@Repository("applicationDao")
public class ApplicationDaoImpl extends AbstractHibernateDao<Application, Long> implements ApplicationDao {
}
