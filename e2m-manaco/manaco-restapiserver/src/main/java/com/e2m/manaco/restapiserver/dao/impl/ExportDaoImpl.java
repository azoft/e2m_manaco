package com.e2m.manaco.restapiserver.dao.impl;

import org.springframework.stereotype.Repository;

import com.e2m.manaco.commonmodule.model.Export;
import com.e2m.manaco.hibernatemodule.dao.impl.AbstractHibernateDao;
import com.e2m.manaco.restapiserver.dao.ExportDao;

@Repository("exportDao")
public class ExportDaoImpl extends AbstractHibernateDao<Export, Long> implements ExportDao {
}
