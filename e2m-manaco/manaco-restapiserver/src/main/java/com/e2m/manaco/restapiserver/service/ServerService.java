package com.e2m.manaco.restapiserver.service;

import com.e2m.manaco.commonmodule.model.Server;
import com.e2m.manaco.hibernatemodule.service.HibernateService;

public interface ServerService extends HibernateService<Server, Long> {
}
