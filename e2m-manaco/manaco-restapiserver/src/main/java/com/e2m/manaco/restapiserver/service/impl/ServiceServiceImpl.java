package com.e2m.manaco.restapiserver.service.impl;

import org.springframework.stereotype.Service;

import com.e2m.manaco.hibernatemodule.service.impl.AbstractHibernateService;
import com.e2m.manaco.restapiserver.dao.impl.ServiceDaoImpl;
import com.e2m.manaco.restapiserver.service.ServiceService;

@Service("serviceService")
public class ServiceServiceImpl extends AbstractHibernateService<com.e2m.manaco.commonmodule.model.Service, Long, ServiceDaoImpl> implements ServiceService {
}
