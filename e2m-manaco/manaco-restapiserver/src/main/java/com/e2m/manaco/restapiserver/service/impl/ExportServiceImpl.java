package com.e2m.manaco.restapiserver.service.impl;

import org.springframework.stereotype.Service;

import com.e2m.manaco.commonmodule.model.Export;
import com.e2m.manaco.hibernatemodule.service.impl.AbstractHibernateService;
import com.e2m.manaco.restapiserver.dao.impl.ExportDaoImpl;
import com.e2m.manaco.restapiserver.service.ExportService;

@Service("exportService")
public class ExportServiceImpl extends AbstractHibernateService<Export, Long, ExportDaoImpl> implements ExportService {
}
