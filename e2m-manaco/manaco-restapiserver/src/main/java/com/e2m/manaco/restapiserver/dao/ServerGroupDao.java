package com.e2m.manaco.restapiserver.dao;

import com.e2m.manaco.commonmodule.model.ServerGroup;
import com.e2m.manaco.hibernatemodule.dao.HibernateDao;

public interface ServerGroupDao extends HibernateDao<ServerGroup, Long> {
}
