package com.e2m.manaco.restapiserver.dao.impl;

import org.springframework.stereotype.Repository;

import com.e2m.manaco.commonmodule.model.ServerGroup;
import com.e2m.manaco.hibernatemodule.dao.impl.AbstractHibernateDao;
import com.e2m.manaco.restapiserver.dao.ServerGroupDao;

@Repository("serverGroupDao")
public class ServerGroupDaoImpl extends AbstractHibernateDao<ServerGroup, Long> implements ServerGroupDao {
}
