package com.e2m.manaco.restapiserver.service.impl;

import org.springframework.stereotype.Service;

import com.e2m.manaco.commonmodule.model.ServerGroup;
import com.e2m.manaco.hibernatemodule.service.impl.AbstractHibernateService;
import com.e2m.manaco.restapiserver.dao.impl.ServerGroupDaoImpl;
import com.e2m.manaco.restapiserver.service.ServerGroupService;

@Service("serverGroupService")
public class ServerGroupServiceImpl extends AbstractHibernateService<ServerGroup, Long, ServerGroupDaoImpl> implements ServerGroupService {
}
