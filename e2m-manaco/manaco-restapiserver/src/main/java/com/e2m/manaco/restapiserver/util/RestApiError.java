package com.e2m.manaco.restapiserver.util;

import org.springframework.http.HttpStatus;

public enum RestApiError {

    ACCESS_IS_DENIED(HttpStatus.FORBIDDEN, "Access is denied"),
    USER_NOT_UNAUTHORIZED(HttpStatus.UNAUTHORIZED, "User not authorized"),
    EXPIRED_SESSION(HttpStatus.UNAUTHORIZED, "Expired session"),
    NOT_FOUND(HttpStatus.NOT_FOUND, "Not found"),

    APPLICATION_NOT_FOUND(HttpStatus.NOT_FOUND, "Application not found (applicationId: %d)"),
    EXPORT_NOT_FOUND(HttpStatus.NOT_FOUND, "Export not found (exportId: %d)"),
    SERVER_NOT_FOUND(HttpStatus.NOT_FOUND, "Server not found (serverId: %d)"),
    SERVER_GROUP_NOT_FOUND(HttpStatus.NOT_FOUND, "Server Group not found (serverGroupId: %d)"),
    SERVICE_NOT_FOUND(HttpStatus.NOT_FOUND, "Service not found (serviceId: %d)");

    private HttpStatus status;

    private String format;

    RestApiError(HttpStatus status, String format) {
        this.status = status;
        this.format = format;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getFormat() {
        return format;
    }

    @Override
    public String toString() {
        return "RestApiError{"
                + "name='" + name() + '\''
                + ", status='" + status.name() + '\''
                + ", code=" + status.value()
                + ", format='" + format + '\''
                + '}';
    }
}
