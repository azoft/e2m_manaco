package com.e2m.manaco.restapiserver.controller;

import static com.e2m.manaco.restapiserver.controller.ServerController.URL_V1_SERVER;
import static com.e2m.manaco.restapiserver.controller.ServerGroupController.URL_V1_SERVER_GROUP;
import static com.e2m.manaco.restapiserver.controller.ServerGroupController.VAR_SERVER_GROUP_ID;
import static com.googlecode.genericdao.search.Filter.OP_EQUAL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.e2m.manaco.commonmodule.model.Server;
import com.e2m.manaco.hibernatemodule.dto.ListRequest;
import com.e2m.manaco.hibernatemodule.dto.ListResult;
import com.e2m.manaco.restapiserver.service.ServerService;
import com.e2m.manaco.restapiserver.util.RestApiError;

@RestController
@RequestMapping(value = URL_V1_SERVER)
public class ServerController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(ServerController.class);

    static final String URL_SERVER = "/server";
    static final String VAR_SERVER_ID = "/{serverId}";

    static final String URL_V1_SERVER = URL_V1_SERVER_GROUP + VAR_SERVER_GROUP_ID + URL_SERVER;

    @Autowired
    private ServerService serverService;

    @GetMapping
    public ResponseEntity findServers(@PathVariable Long serverGroupId) {
        ListRequest listRequest = new ListRequest();
        listRequest.addFilter("group.id", serverGroupId, OP_EQUAL);
        ListResult<Server> servers = serverService.find(listRequest);
        log.info("findServers, serverGroupId: {}, result: {}", serverGroupId, servers);
        return createResponseEntity(servers);
    }

    @GetMapping(VAR_SERVER_ID)
    public ResponseEntity getServer(@PathVariable Long serverGroupId, @PathVariable Long serverId) {
        Server server = serverService.get(serverId);
        log.info("getServer, serverGroupId: {}, serverId: {}, result: {}", serverGroupId, serverId, server);
        if (server == null) {
            return createResponseEntity(RestApiError.SERVER_NOT_FOUND, serverId);
        }
        return createResponseEntity(server);
    }
}
