package com.e2m.manaco.restapiserver.service.impl;

import org.springframework.stereotype.Service;

import com.e2m.manaco.commonmodule.model.Server;
import com.e2m.manaco.hibernatemodule.service.impl.AbstractHibernateService;
import com.e2m.manaco.restapiserver.dao.impl.ServerDaoImpl;
import com.e2m.manaco.restapiserver.service.ServerService;

@Service("serverService")
public class ServerServiceImpl extends AbstractHibernateService<Server, Long, ServerDaoImpl> implements ServerService {
}
