package com.e2m.manaco.restapiserver.dao.impl;

import org.springframework.stereotype.Repository;

import com.e2m.manaco.commonmodule.model.Server;
import com.e2m.manaco.hibernatemodule.dao.impl.AbstractHibernateDao;
import com.e2m.manaco.restapiserver.dao.ServerDao;

@Repository("serverDao")
public class ServerDaoImpl extends AbstractHibernateDao<Server, Long> implements ServerDao {
}
