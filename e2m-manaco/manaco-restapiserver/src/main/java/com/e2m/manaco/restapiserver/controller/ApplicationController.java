package com.e2m.manaco.restapiserver.controller;

import static com.e2m.manaco.restapiserver.controller.ApplicationController.URL_V1_APPLICATION;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.e2m.manaco.commonmodule.model.Application;
import com.e2m.manaco.hibernatemodule.dto.ListResult;
import com.e2m.manaco.restapiserver.service.ApplicationService;
import com.e2m.manaco.restapiserver.util.RestApiError;

@RestController
@RequestMapping(value = URL_V1_APPLICATION)
public class ApplicationController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(ApplicationController.class);

    static final String URL_V1_APPLICATION = URL_V1 + "/application";
    static final String VAR_APPLICATION_ID = "/{applicationId}";

    @Autowired
    private ApplicationService applicationService;

    @GetMapping
    public ResponseEntity findApplications() {
        ListResult<Application> applications = applicationService.findAll();
        log.info("findApplications, result: {}", applications);
        return createResponseEntity(applications);
    }

    @GetMapping(VAR_APPLICATION_ID)
    public ResponseEntity getApplication(@PathVariable Long applicationId) {
        Application application = applicationService.get(applicationId);
        log.info("getApplication applicationId: {}, result: {}", applicationId, application);
        if (application == null) {
            return createResponseEntity(RestApiError.APPLICATION_NOT_FOUND, applicationId);
        }
        return createResponseEntity(application);
    }
}
