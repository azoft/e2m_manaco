package com.e2m.manaco.restapiserver.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.e2m.manaco.restapiserver.util.RestApiError;

@RestController
public class RestApiErrorController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(RestApiErrorController.class);

    @RequestMapping("/accessdenied")
    public ResponseEntity accessdenied() {
        return createResponseEntity(RestApiError.ACCESS_IS_DENIED);
    }

    @RequestMapping("/unauthorized")
    public ResponseEntity unauthorized() {
        return createResponseEntity(RestApiError.USER_NOT_UNAUTHORIZED);
    }

    @RequestMapping("/sessionexpired")
    public ResponseEntity sessionexpired() {
        return createResponseEntity(RestApiError.EXPIRED_SESSION);
    }

    @RequestMapping("/404")
    public ResponseEntity notFound() {
        return createResponseEntity(RestApiError.NOT_FOUND);
    }
}
