package com.e2m.manaco.restapiserver.controller;

import static com.e2m.manaco.restapiserver.controller.ExportController.URL_V1_EXPORT;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.e2m.manaco.commonmodule.model.Export;
import com.e2m.manaco.hibernatemodule.dto.ListResult;
import com.e2m.manaco.restapiserver.dto.RequestData;
import com.e2m.manaco.restapiserver.service.ExportService;
import com.e2m.manaco.restapiserver.util.RestApiError;

@RestController
@RequestMapping(value = URL_V1_EXPORT)
public class ExportController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(ExportController.class);

    static final String URL_V1_EXPORT = URL_V1 + "/export";
    static final String VAR_EXPORT_ID = "/{exportId}";

    private static final String STATUS_NEW = "NEW";
    private static final String OWNER_ADMIN_1 = "admin1";

    @Autowired
    private ExportService exportService;

    @GetMapping
    public ResponseEntity findExports() {
        ListResult<Export> exports = exportService.findAll();
        log.info("findExports, result: {}", exports);
        return createResponseEntity(exports);
    }

    @GetMapping(VAR_EXPORT_ID)
    public ResponseEntity getExport(@PathVariable Long exportId) {
        Export export = exportService.get(exportId);
        log.info("getExport exportId: {}, result: {}", exportId, export);
        if (export == null) {
            return createResponseEntity(RestApiError.EXPORT_NOT_FOUND, exportId);
        }
        return createResponseEntity(export);
    }

    @PostMapping
    public ResponseEntity createExport(@RequestBody RequestData<Export> requestData) {
        Export export = requestData.getData();
        //TODO createExport
        export.setOwner(OWNER_ADMIN_1);
        export.setStatus(STATUS_NEW);
        exportService.create(export);
        log.info("createExport export: {}", export);
        return createResponseEntity(export);
    }

    @DeleteMapping(VAR_EXPORT_ID)
    public ResponseEntity deleteExport(@PathVariable Long exportId) {
        //TODO deleteExport
        Boolean result = exportService.delete(exportId);
        log.info("deleteExport exportId: {}, result: {}", exportId, result);
        return createResponseEntity(result);
    }
}
