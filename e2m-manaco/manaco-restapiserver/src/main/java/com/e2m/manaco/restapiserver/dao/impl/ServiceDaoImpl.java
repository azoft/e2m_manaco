package com.e2m.manaco.restapiserver.dao.impl;

import org.springframework.stereotype.Repository;

import com.e2m.manaco.commonmodule.model.Service;
import com.e2m.manaco.hibernatemodule.dao.impl.AbstractHibernateDao;
import com.e2m.manaco.restapiserver.dao.ServiceDao;

@Repository("serviceDao")
public class ServiceDaoImpl extends AbstractHibernateDao<Service, Long> implements ServiceDao {
}
