package com.e2m.manaco.restapiserver.dto;

public class RequestData<T> {

    private T data;

    public RequestData() {
    }

    public RequestData(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RequestData{"
                + "data=" + getData()
                + '}';
    }
}
