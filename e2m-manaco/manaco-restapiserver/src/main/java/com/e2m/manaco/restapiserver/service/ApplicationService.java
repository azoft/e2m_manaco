package com.e2m.manaco.restapiserver.service;

import com.e2m.manaco.commonmodule.model.Application;
import com.e2m.manaco.hibernatemodule.service.HibernateService;

public interface ApplicationService extends HibernateService<Application, Long> {
}
