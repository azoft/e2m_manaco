package com.e2m.manaco.restapiserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:manaco-restapiserver.xml")
public class RestApiServerApplication {

    private static final Logger log = LoggerFactory.getLogger(RestApiServerApplication.class);

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(RestApiServerApplication.class, args);

        log.info("Starting up");
        //final ManacoService manacoService = context.getBean(ManacoService.class);
    }
}
