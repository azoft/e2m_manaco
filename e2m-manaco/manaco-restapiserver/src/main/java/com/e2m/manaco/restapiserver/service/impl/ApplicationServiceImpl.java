package com.e2m.manaco.restapiserver.service.impl;

import org.springframework.stereotype.Service;

import com.e2m.manaco.commonmodule.model.Application;
import com.e2m.manaco.hibernatemodule.service.impl.AbstractHibernateService;
import com.e2m.manaco.restapiserver.dao.impl.ApplicationDaoImpl;
import com.e2m.manaco.restapiserver.service.ApplicationService;

@Service("applicationService")
public class ApplicationServiceImpl extends AbstractHibernateService<Application, Long, ApplicationDaoImpl> implements ApplicationService {
}
