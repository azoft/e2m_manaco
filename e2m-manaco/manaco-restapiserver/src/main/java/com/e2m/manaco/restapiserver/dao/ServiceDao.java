package com.e2m.manaco.restapiserver.dao;

import com.e2m.manaco.commonmodule.model.Service;
import com.e2m.manaco.hibernatemodule.dao.HibernateDao;

public interface ServiceDao extends HibernateDao<Service, Long> {
}
