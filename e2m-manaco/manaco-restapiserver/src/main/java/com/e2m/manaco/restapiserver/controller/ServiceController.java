package com.e2m.manaco.restapiserver.controller;

import static com.e2m.manaco.restapiserver.controller.ServerController.URL_SERVER;
import static com.e2m.manaco.restapiserver.controller.ServerController.VAR_SERVER_ID;
import static com.e2m.manaco.restapiserver.controller.ServiceController.URL_V1_SERVICE;
import static com.googlecode.genericdao.search.Filter.OP_EQUAL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.e2m.manaco.commonmodule.model.Service;
import com.e2m.manaco.hibernatemodule.dto.ListRequest;
import com.e2m.manaco.hibernatemodule.dto.ListResult;
import com.e2m.manaco.restapiserver.service.ServiceService;
import com.e2m.manaco.restapiserver.util.RestApiError;

@RestController
@RequestMapping(value = URL_V1_SERVICE)
public class ServiceController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(ServiceController.class);

    static final String URL_SERVICE = "/service";
    static final String URL_ACTION = "/action";
    static final String VAR_SERVICE_ID = "/{serviceId}";
    static final String VAR_ACTION_CODE = "/{actionCode}";

    static final String URL_V1_SERVICE = URL_V1 + URL_SERVER + VAR_SERVER_ID + URL_SERVICE;

    @Autowired
    private ServiceService serviceService;

    @GetMapping
    public ResponseEntity findServices(@PathVariable Long serverId) {
        ListRequest listRequest = new ListRequest();
        listRequest.addFilter("server.id", serverId, OP_EQUAL);
        ListResult<Service> services = serviceService.find(listRequest);
        log.info("findServices, result: {}", services);
        return createResponseEntity(services);
    }

    @GetMapping(VAR_SERVICE_ID)
    public ResponseEntity getService(@PathVariable Long serverId, @PathVariable Long serviceId) {
        Service service = serviceService.get(serviceId);
        log.info("getService serverId: {}, serviceId: {}, result: {}", serverId, serviceId, service);
        if (service == null) {
            return createResponseEntity(RestApiError.SERVICE_NOT_FOUND, serviceId);
        }
        return createResponseEntity(service);
    }

    @PostMapping(VAR_SERVICE_ID + URL_ACTION + VAR_ACTION_CODE)
    public ResponseEntity actionService(@PathVariable Long serverId, @PathVariable Long serviceId, @PathVariable String actionCode) {
        Service service = serviceService.get(serviceId);
        if (service == null) {
            return createResponseEntity(RestApiError.SERVICE_NOT_FOUND, serviceId);
        }
        //TODO actionService
        service.setStatus(actionCode);
        serviceService.update(service);
        log.info("actionService serverId: {}, serviceId: {}, actionCode: {}, result: {}", serverId, serviceId, actionCode, service);
        return createResponseEntity(service);
    }
}
