package com.e2m.manaco.hibernatemodule.dao;

import com.e2m.manaco.hibernatemodule.model.TestUser;

public interface TestUserDao extends HibernateDao<TestUser, Long> {
}
