package com.e2m.manaco.hibernatemodule.service;

import com.e2m.manaco.hibernatemodule.model.TestUser;

public interface TestUserService extends HibernateService<TestUser, Long> {
}
