package com.e2m.manaco.hibernatemodule.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import com.googlecode.genericdao.search.Sort;

public class AbstractHibernateDaoTest {

    @Test
    public void createSort() throws Exception {
        Sort sort = AbstractHibernateDao.createSort("id", "asc");
        assertNotNull(sort);
        assertEquals("id", sort.getProperty());
        assertFalse(sort.isDesc());

        sort = AbstractHibernateDao.createSort("userId", "desc");
        assertNotNull(sort);
        assertEquals("userId", sort.getProperty());
        assertTrue(sort.isDesc());

        sort = AbstractHibernateDao.createSort("name", null);
        assertNotNull(sort);
        assertEquals("name", sort.getProperty());
        assertFalse(sort.isDesc());
    }

    @Test
    public void testCreateSorts() throws Exception {
        List<Sort> sorts = AbstractHibernateDao.createSorts(null, null);
        assertNotNull(sorts);
        assertEquals(0, sorts.size());

        sorts = AbstractHibernateDao.createSorts("id", "desc");
        assertNotNull(sorts);
        assertEquals(1, sorts.size());
        assertEquals("id", sorts.get(0).getProperty());
        assertTrue(sorts.get(0).isDesc());

        sorts = AbstractHibernateDao.createSorts("id", "asc");
        assertNotNull(sorts);
        assertEquals(1, sorts.size());
        assertEquals("id", sorts.get(0).getProperty());
        assertFalse(sorts.get(0).isDesc());

        sorts = AbstractHibernateDao.createSorts("id,name", "desc,asc");
        assertNotNull(sorts);
        assertEquals(2, sorts.size());
        assertEquals("id", sorts.get(0).getProperty());
        assertTrue(sorts.get(0).isDesc());
        assertEquals("name", sorts.get(1).getProperty());
        assertFalse(sorts.get(1).isDesc());

        sorts = AbstractHibernateDao.createSorts(" id, name ", " asc, asc ");
        assertNotNull(sorts);
        assertEquals(2, sorts.size());
        assertEquals("id", sorts.get(0).getProperty());
        assertFalse(sorts.get(0).isDesc());
        assertEquals("name", sorts.get(1).getProperty());
        assertFalse(sorts.get(1).isDesc());

        sorts = AbstractHibernateDao.createSorts(null, "wesc");
        assertNull(sorts);
        sorts = AbstractHibernateDao.createSorts("id", null);
        assertNull(sorts);
        sorts = AbstractHibernateDao.createSorts("id", "wesc");
        assertNull(sorts);
        sorts = AbstractHibernateDao.createSorts(null, "desc,asc");
        assertNull(sorts);
        sorts = AbstractHibernateDao.createSorts("id,name", null);
        assertNull(sorts);
        sorts = AbstractHibernateDao.createSorts("id,name", "desc,rsc");
        assertNull(sorts);
        sorts = AbstractHibernateDao.createSorts("id,name", "desc");
        assertNull(sorts);
        sorts = AbstractHibernateDao.createSorts("id", "desc,asc");
        assertNull(sorts);
    }
}