package com.e2m.manaco.hibernatemodule.util;

class IncValue {

    private Long value = 0L;

    Long incValue() {
        return ++value;
    }
}
