package com.e2m.manaco.hibernatemodule.dao.impl;

import org.springframework.stereotype.Repository;

import com.e2m.manaco.hibernatemodule.dao.TestUserDao;
import com.e2m.manaco.hibernatemodule.model.TestUser;

@Repository("testUserDao")
public class TestUserDaoImpl extends AbstractHibernateDao<TestUser, Long> implements TestUserDao {
}
