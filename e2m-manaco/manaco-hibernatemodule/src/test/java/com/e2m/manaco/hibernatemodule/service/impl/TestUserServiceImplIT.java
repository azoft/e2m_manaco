package com.e2m.manaco.hibernatemodule.service.impl;

import static com.e2m.manaco.hibernatemodule.util.TestUserUtil.TEST_USERID_1;
import static com.e2m.manaco.hibernatemodule.util.TestUserUtil.TEST_USERID_2;
import static com.e2m.manaco.hibernatemodule.util.TestUserUtil.TEST_USERID_3;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.e2m.manaco.hibernatemodule.ITSpring;
import com.e2m.manaco.hibernatemodule.dto.ListRequest;
import com.e2m.manaco.hibernatemodule.dto.ListResult;
import com.e2m.manaco.hibernatemodule.model.TestUser;
import com.e2m.manaco.hibernatemodule.service.TestUserService;
import com.e2m.manaco.hibernatemodule.util.TestUserUtil;
import com.googlecode.genericdao.search.Filter;

public class TestUserServiceImplIT extends ITSpring {

    @Autowired
    private TestUserService testUserService;

    private List<TestUser> users;

    @Before
    public void before() {
        users = TestUserUtil.createTestUsers();
        users.forEach(u -> testUserService.create(u));
    }

    @After
    public void after() {
        testUserService.clear();
    }

    @Test
    public void get() {
        TestUser etalonUser = users.get(0);
        TestUser user = testUserService.get(etalonUser.getId());
        assertNotNull(user);
        assertEquals(etalonUser, user);
    }

    @Test
    public void searchUnique() {
        TestUser user = testUserService.searchUnique("userId", TEST_USERID_2);
        assertNotNull(user);
        assertEquals(TEST_USERID_2, user.getUserId());
        assertEquals("A User", user.getName());
        assertNull(user.getAge());

        user = testUserService.searchUnique("name", "B User");
        assertNotNull(user);
        assertEquals(TEST_USERID_3, user.getUserId());
        assertEquals("B User", user.getName());
        assertEquals(Integer.valueOf(35), user.getAge());

        user = testUserService.searchUnique("name", "B User 1");
        assertNull(user);
    }

    @Test
    public void searchUnique_byFilters() {
        List<Filter> filters = createFilters(TEST_USERID_3, "B User", 35);
        TestUser user = testUserService.searchUnique(filters);
        assertNotNull(user);
        assertEquals(TEST_USERID_3, user.getUserId());
        assertEquals("B User", user.getName());
        assertEquals(Integer.valueOf(35), user.getAge());

        filters = createFilters(TEST_USERID_3, "B User 1", 35);
        user = testUserService.searchUnique(filters);
        assertNull(user);
    }

    private List<Filter> createFilters(String userId, String name, Integer age) {
        List<Filter> filters = new ArrayList<>();
        filters.add(new Filter("userId", userId));
        filters.add(new Filter("name", name));
        filters.add(new Filter("age", age));
        return filters;
    }

    @Test
    public void find() {
        ListResult<TestUser> listResult = testUserService.find(new ListRequest());
        assertFindAll(listResult);

        ListRequest listRequest = new ListRequest(2, 1, "name", "desc");
        listResult = testUserService.find(listRequest);

        assertNotNull(listResult);
        assertEquals(Integer.valueOf(2), listResult.getCount());
        assertEquals(Integer.valueOf(3), listResult.getTotalCount());
        assertEquals("name", listResult.getSort());
        assertEquals("desc", listResult.getOrder());
        assertEquals(Integer.valueOf(2), listResult.getLimit());
        assertEquals(Integer.valueOf(1), listResult.getOffset());
        assertNotNull(listResult.getFilters());
        assertEquals(0, listResult.getFilters().size());
        List<TestUser> testUsers = listResult.getList();
        assertNotNull(testUsers);
        assertEquals(2, testUsers.size());

        assertEquals(TEST_USERID_3, testUsers.get(0).getUserId());
        assertEquals(TEST_USERID_2, testUsers.get(1).getUserId());
    }

    @Test
    public void findFirst() {
        TestUser result = testUserService.findFirst(new ListRequest());
        assertNotNull(result);
        assertEquals(TestUserUtil.TEST_USERID_1, result.getUserId());

        ListRequest listRequest = new ListRequest(2, 1, "name", "desc");
        result = testUserService.findFirst(listRequest);
        assertNotNull(result);
        assertEquals(TestUserUtil.TEST_USERID_3, result.getUserId());
    }

    @Test
    public void findAll() {
        ListResult<TestUser> listResult = testUserService.findAll();
        assertFindAll(listResult);
    }

    private void assertFindAll(ListResult<TestUser> listResult) {
        assertNotNull(listResult);
        assertEquals(Integer.valueOf(3), listResult.getCount());
        assertEquals(Integer.valueOf(3), listResult.getTotalCount());
        assertNull(listResult.getSort());
        assertNull(listResult.getOrder());
        assertNull(listResult.getLimit());
        assertNull(listResult.getOffset());
        assertNotNull(listResult.getFilters());
        assertEquals(0, listResult.getFilters().size());
        List<TestUser> testUsers = listResult.getList();
        assertNotNull(testUsers);
        assertEquals(3, testUsers.size());

        assertEquals(TEST_USERID_1, testUsers.get(0).getUserId());
        assertEquals(TEST_USERID_2, testUsers.get(1).getUserId());
        assertEquals(TEST_USERID_3, testUsers.get(2).getUserId());
    }

    @Test
    public void count() {
        assertEquals(3, testUserService.count());
    }

    @Test
    public void update() {
        TestUser etalonUser = users.get(0);
        etalonUser.setName("New name");
        testUserService.update(etalonUser);

        TestUser user = testUserService.get(etalonUser.getId());
        assertNotNull(user);
        assertEquals("New name", user.getName());
    }

    @Test
    public void create() {
        assertEquals(3, testUserService.count());
        testUserService.create(TestUserUtil.createTestUser_4());
        assertEquals(4, testUserService.count());
    }

    @Test
    public void delete() {
        assertEquals(3, testUserService.count());
        TestUser etalonUser = users.get(0);
        testUserService.delete(etalonUser.getId());
        assertEquals(2, testUserService.count());
    }

    @Test
    public void clear() {
        assertEquals(3, testUserService.count());
        int result = testUserService.clear();
        assertEquals(3, result);
        assertEquals(0, testUserService.count());
    }

    @Test
    public void convertToListIds() {
        List<TestUser> users = TestUserUtil.getFilledTestUsers();
        List<Long> ids = testUserService.convertToListIds(users);
        assertNotNull(ids);
        assertEquals(3, ids.size());
        assertEquals(Long.valueOf(1), ids.get(0));
        assertEquals(Long.valueOf(2), ids.get(1));
        assertEquals(Long.valueOf(3), ids.get(2));
    }

    @Test
    public void convertToStringIds() {
        List<TestUser> users = TestUserUtil.getFilledTestUsers();
        String ids = testUserService.convertToStringIds(users);
        assertNotNull(ids);
        assertEquals("(1,2,3)", ids);
    }
}