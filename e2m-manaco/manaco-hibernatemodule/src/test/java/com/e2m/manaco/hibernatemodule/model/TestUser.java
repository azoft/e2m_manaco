package com.e2m.manaco.hibernatemodule.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "test_user")
public class TestUser extends AbstractHibernateEntity<Long> {

    private static final String USER_ID_COLUMN = "userId";
    private static final String NAME_COLUMN = "name";
    private static final int NAME_LENGTH = 255;
    private static final String AGE_COLUMN = "age";

    @Column(name = USER_ID_COLUMN, nullable = false)
    private String userId;

    @Column(name = NAME_COLUMN, nullable = false, length = NAME_LENGTH)
    private String name;

    @Column(name = AGE_COLUMN)
    private Integer age;

    public TestUser() {
        super();
    }

    public TestUser(String userId, String name, Integer age) {
        this();
        this.userId = userId;
        this.name = name;
        this.age = age;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TestUser)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        TestUser user = (TestUser) o;

        if (!userId.equals(user.userId)) {
            return false;
        }
        if (!name.equals(user.name)) {
            return false;
        }
        return age != null ? age.equals(user.age) : user.age == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + userId.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + (age != null ? age.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TestUser{"
                + "Id=" + getId()
                + ", userId=" + userId
                + ", name='" + name + '\''
                + ", age=" + age
                + '}';
    }
}
