package com.e2m.manaco.hibernatemodule.service.impl;

import org.springframework.stereotype.Service;

import com.e2m.manaco.hibernatemodule.dao.impl.TestUserDaoImpl;
import com.e2m.manaco.hibernatemodule.model.TestUser;
import com.e2m.manaco.hibernatemodule.service.TestUserService;

@Service("testUserService")
public class TestUserServiceImpl extends AbstractHibernateService<TestUser, Long, TestUserDaoImpl> implements TestUserService {
}
