package com.e2m.manaco.hibernatemodule.util;

import java.util.ArrayList;
import java.util.List;

import com.e2m.manaco.hibernatemodule.model.TestUser;

public final class TestUserUtil {

    public static final String TEST_USERID_1 = "1001";
    public static final String TEST_USERID_2 = "1002";
    public static final String TEST_USERID_3 = "1003";
    public static final String TEST_USERID_4 = "1004";

    private TestUserUtil() {
    }

    public static TestUser createTestUser_1() {
        return new TestUser(TEST_USERID_1, "C User", 25);
    }

    public static TestUser createTestUser_2() {
        return new TestUser(TEST_USERID_2, "A User", null);
    }

    public static TestUser createTestUser_3() {
        return new TestUser(TEST_USERID_3, "B User", 35);
    }

    public static TestUser createTestUser_4() {
        return new TestUser(TEST_USERID_4, "D User", 30);
    }

    public static List<TestUser> createTestUsers() {
        List<TestUser> users = new ArrayList<>();
        users.add(createTestUser_1());
        users.add(createTestUser_2());
        users.add(createTestUser_3());
        return users;
    }

    public static List<TestUser> getFilledTestUsers() {
        List<TestUser> users = TestUserUtil.createTestUsers();
        IncValue incValue = new IncValue();
        users.forEach(u -> u.setId(incValue.incValue()));
        return users;
    }
}
