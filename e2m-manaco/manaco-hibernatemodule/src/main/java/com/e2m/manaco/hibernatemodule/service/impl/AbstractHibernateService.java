package com.e2m.manaco.hibernatemodule.service.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.e2m.manaco.hibernatemodule.dao.impl.AbstractHibernateDao;
import com.e2m.manaco.hibernatemodule.dto.ListRequest;
import com.e2m.manaco.hibernatemodule.dto.ListResult;
import com.e2m.manaco.hibernatemodule.model.HibernateEntityIdentifiable;
import com.e2m.manaco.hibernatemodule.service.HibernateService;
import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;

public abstract class AbstractHibernateService<T extends HibernateEntityIdentifiable<ID>, ID extends Serializable, DAO extends AbstractHibernateDao<T, ID>>
        implements HibernateService<T, ID> {

    @Autowired
    private DAO dao;

    protected DAO getDao() {
        return dao;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public T get(ID id) {
        return getDao().fetch(id);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public T searchUnique(String property, Object value) {
        return getDao().searchUnique(property, value);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public T searchUnique(List<Filter> filters) {
        return getDao().searchUnique(filters);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public ListResult<T> find(ListRequest listRequest) {
        return getDao().find(listRequest);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public T findFirst(ListRequest listRequest) {
        return getDao().findFirst(listRequest);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public ListResult<T> findAll() {
        Search search = new Search();
        search.addSort("id", false);
        List<T> list = getDao().search(search);
        ListResult<T> listResult = new ListResult<>();
        listResult.setList(list);
        int size = list.size();
        listResult.setCount(size);
        listResult.setTotalCount(size);
        return listResult;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public int count() {
        return getDao().count(null);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public boolean update(T object) {
        return getDao().createOrUpdate(object);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void create(T object) {
        getDao().create(object);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public boolean delete(ID id) {
        return getDao().deleteById(id);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public boolean delete(T object) {
        return getDao().deleteEntity(object);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int clear() {
        return getDao().clear();
    }

    @Override
    public List<ID> convertToListIds(Collection<T> entities) {
        return entities.stream().map(HibernateEntityIdentifiable::getId).collect(Collectors.toList());
    }

    @Override
    public String convertToStringIds(Collection<T> entities) {
        return "(" + entities.stream()
                .map(u -> u.getId().toString())
                .collect(Collectors.joining(",")) + ")";
    }
}
