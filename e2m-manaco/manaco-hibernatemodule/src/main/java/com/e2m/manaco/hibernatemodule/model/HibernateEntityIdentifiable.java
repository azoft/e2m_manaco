package com.e2m.manaco.hibernatemodule.model;

import java.io.Serializable;

public interface HibernateEntityIdentifiable<ID extends Serializable> extends Serializable {

    ID getId();
}
