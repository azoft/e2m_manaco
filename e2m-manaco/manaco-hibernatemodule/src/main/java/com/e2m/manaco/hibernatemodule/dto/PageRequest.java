package com.e2m.manaco.hibernatemodule.dto;

public class PageRequest implements Pageable {

    private Integer limit;

    private Integer offset;

    private String sort;

    private String order;

    /**
     * @param limit  - лимит возвращаемых элементов
     * @param offset - оффсет
     * @param sort   - название поля для сортировки, может быть несколько сортировок, разделяются запятой
     * @param order  - направление сортировки, может быть несколько сортировок, разделяются запятой
     */
    public PageRequest(Integer limit, Integer offset, String sort, String order) {
        this.sort = sort;
        this.order = order;
        this.limit = limit;
        this.offset = offset;
    }

    /**
     * @param limit  - лимит возвращаемых элементов
     * @param offset - оффсет
     */
    public PageRequest(Integer limit, Integer offset) {
        this.limit = limit;
        this.offset = offset;
    }

    /**
     * @param sort  - название поля для сортировки, может быть несколько сортировок, разделяются запятой
     * @param order - направление сортировки, может быть несколько сортировок, разделяются запятой
     */
    public PageRequest(String sort, String order) {
        this(null, null, sort, order);
    }

    public PageRequest(Pageable pageable) {
        this(pageable.getLimit(), pageable.getOffset(), pageable.getSort(), pageable.getOrder());
    }

    public PageRequest() {

    }

    @Override
    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    @Override
    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    @Override
    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    @Override
    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "PageRequest{"
                + "limit=" + limit
                + ", offset=" + offset
                + ", sort='" + sort + '\''
                + ", order='" + order + '\''
                + '}';
    }
}
