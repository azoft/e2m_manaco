package com.e2m.manaco.hibernatemodule.dao;

import java.io.Serializable;
import java.util.List;

import com.e2m.manaco.hibernatemodule.dto.ListRequest;
import com.e2m.manaco.hibernatemodule.dto.ListResult;
import com.googlecode.genericdao.dao.hibernate.original.GenericDAO;
import com.googlecode.genericdao.search.Filter;

public interface HibernateDao<T, ID extends Serializable> extends GenericDAO<T, ID> {

    T searchUnique(String property, Object value);

    T searchUnique(List<Filter> filters);

    ListResult<T> find(ListRequest listRequest);

    T findFirst(ListRequest listRequest);

    int clear();
}
