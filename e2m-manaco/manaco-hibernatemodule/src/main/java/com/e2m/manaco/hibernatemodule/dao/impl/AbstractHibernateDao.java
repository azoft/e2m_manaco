package com.e2m.manaco.hibernatemodule.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.util.StringUtils;

import com.e2m.manaco.hibernatemodule.dao.HibernateDao;
import com.e2m.manaco.hibernatemodule.dto.ListRequest;
import com.e2m.manaco.hibernatemodule.dto.ListResult;
import com.googlecode.genericdao.dao.hibernate.original.GenericDAOImpl;
import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.Search;
import com.googlecode.genericdao.search.SearchResult;
import com.googlecode.genericdao.search.Sort;

public abstract class AbstractHibernateDao<T, ID extends Serializable> extends GenericDAOImpl<T, ID> implements HibernateDao<T, ID> {

    public static final String ASC_DIR = "asc";

    public static final String DESC_DIR = "desc";

    public static final String PARAM_DELIMITER = ",";

    @Override
    @Resource
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    public static Sort createSort(String sortField, String sortDir) {
        Sort sort = new Sort();
        sort.setProperty(sortField);
        sort.setDesc(DESC_DIR.equals(sortDir));
        return sort;
    }

    public static List<Sort> createSorts(String sortField, String sortDir) {
        List<Sort> sorts = new ArrayList<>();
        if (!StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortDir)) {
            String[] sortFieldArray = sortField.split(PARAM_DELIMITER);
            String[] sortDirArray = sortDir.split(PARAM_DELIMITER);
            if (sortFieldArray.length == sortDirArray.length) {
                for (int i = 0, sortFieldArrayLength = sortFieldArray.length; i < sortFieldArrayLength; i++) {
                    String sf = sortFieldArray[i].trim();
                    String sd = sortDirArray[i].trim().toLowerCase();
                    if (!StringUtils.isEmpty(sf) && !StringUtils.isEmpty(sd)
                            && (ASC_DIR.equals(sd) || DESC_DIR.equals(sd))) {
                        Sort sort = createSort(sf, sd);
                        if (sort != null) {
                            sorts.add(sort);
                        }
                    } else {
                        sorts = null;
                        break;
                    }
                }
            } else {
                sorts = null;
            }
        } else {
            if ((StringUtils.isEmpty(sortField) && !StringUtils.isEmpty(sortDir))
                    || (!StringUtils.isEmpty(sortField) && StringUtils.isEmpty(sortDir))) {
                sorts = null;
            }
        }
        return sorts;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T searchUnique(String property, Object value) {
        Filter filter = new Filter(property, value);
        ISearch search = new Search().setSearchClass(persistentClass).addFilter(filter);
        return (T) this._searchUnique(search);
    }

    @Override
    @SuppressWarnings("unchecked")
    public T searchUnique(List<Filter> filters) {
        Search search = new Search().setSearchClass(persistentClass);
        for (Filter filter : filters) {
            search.addFilterAnd(filter);
        }
        return (T) this._searchUnique(search);
    }

    @Override
    public ListResult<T> find(ListRequest listRequest) {
        ListResult<T> listResult =
                new ListResult<>(listRequest.getLimit(), listRequest.getOffset(), listRequest.getSort(), listRequest.getOrder());
        List<Sort> sorts = createSorts(listRequest.getSort(), listRequest.getOrder());
        Search search = new Search();
        if (listRequest.getLimit() != null) {
            search.setMaxResults(listRequest.getLimit());
        }
        if (listRequest.getOffset() != null) {
            search.setFirstResult(listRequest.getOffset());
        }
        addFilters(search, listRequest.getFilters());
        if (sorts != null) {
            search.setSorts(sorts);
        }
        SearchResult<T> searchResult = this.searchAndCount(search);
        listResult.setTotalCount(searchResult.getTotalCount());
        listResult.setCount(searchResult.getResult().size());
        listResult.setList(searchResult.getResult());
        return listResult;
    }

    protected void addFilters(Search search, List<Filter> filters) {
        if (filters != null) {
            for (Filter filter : filters) {
                search.addFilter(filter);
            }
        }
    }

    @Override
    public T findFirst(ListRequest listRequest) {
        ListResult<T> listResult = find(listRequest);
        return listResult.getCount() > 0 ? listResult.getList().get(0) : null;
    }

    @Override
    public int clear() {
        Query query = getSession().createQuery("delete " + persistentClass.getSimpleName());
        return query.executeUpdate();
    }
}
