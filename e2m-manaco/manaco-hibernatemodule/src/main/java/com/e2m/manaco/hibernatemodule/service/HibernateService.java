package com.e2m.manaco.hibernatemodule.service;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import com.e2m.manaco.hibernatemodule.dto.ListRequest;
import com.e2m.manaco.hibernatemodule.dto.ListResult;
import com.e2m.manaco.hibernatemodule.model.HibernateEntityIdentifiable;
import com.googlecode.genericdao.search.Filter;

public interface HibernateService<T extends HibernateEntityIdentifiable<ID>, ID extends Serializable> {

    T get(ID id);

    T searchUnique(String property, Object value);

    T searchUnique(List<Filter> filters);

    ListResult<T> find(ListRequest listRequest);

    T findFirst(ListRequest listRequest);

    ListResult<T> findAll();

    int count();

    boolean update(T object);

    void create(T object);

    boolean delete(T object);

    boolean delete(ID id);

    int clear();

    List<ID> convertToListIds(Collection<T> entities);

    String convertToStringIds(Collection<T> entities);
}
