package com.e2m.manaco.hibernatemodule.dto;

import java.util.ArrayList;
import java.util.List;

public class ListResult<Type> extends ListRequest {

    private Integer totalCount;

    private Integer count;

    private List<Type> list = new ArrayList<Type>();

    public ListResult() {
        super();
    }

    public ListResult(Integer limit, Integer offset, String sort, String order) {
        super(limit, offset, sort, order);
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<Type> getList() {
        return list;
    }

    public void setList(List<Type> list) {
        this.list = list;
    }

    public void addObject(Type object) {
        list.add(object);
    }

    @Override
    public String toString() {
        return "ListResult{"
                + "totalCount=" + totalCount
                + ", count=" + count
                + ", list=" + list
                + "} " + super.toString();
    }
}
