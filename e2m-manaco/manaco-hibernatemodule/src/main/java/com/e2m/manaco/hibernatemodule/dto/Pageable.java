package com.e2m.manaco.hibernatemodule.dto;

public interface Pageable {

    Integer getLimit();

    Integer getOffset();

    String getSort();

    String getOrder();
}
