package com.e2m.manaco.hibernatemodule.dto;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.genericdao.search.Filter;

public class ListRequest extends PageRequest {

    private List<Filter> filters = new ArrayList<>();

    public ListRequest(Integer limit, Integer offset, String sort, String order) {
        super(limit, offset, sort, order);
    }

    public ListRequest(String sort, String order) {
        super(sort, order);
    }

    public ListRequest(Pageable pageable) {
        super(pageable);
    }

    public ListRequest() {
        super();
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public Filter addFilter(String property, Object value, int operator) {
        Filter filter = new Filter(property, value, operator);
        filters.add(filter);
        return filter;
    }

    public Filter addFilter(Filter filter) {
        filters.add(filter);
        return filter;
    }

    @Override
    public String toString() {
        return "ListRequest{"
                + "filters=" + filters
                + "} " + super.toString();
    }
}
