package com.e2m.manaco.commonmodule.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.e2m.manaco.hibernatemodule.model.AbstractHibernateEntity;

@MappedSuperclass
public class AbstractNameEntity extends AbstractHibernateEntity<Long> {

    private static final String NAME_COLUMN = "name";
    private static final int NAME_LENGTH = 255;

    @Column(name = NAME_COLUMN, nullable = false, length = NAME_LENGTH)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
