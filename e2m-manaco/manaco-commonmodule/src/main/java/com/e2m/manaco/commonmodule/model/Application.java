package com.e2m.manaco.commonmodule.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "application")
public class Application extends AbstractNameEntity {

    @Override
    public String toString() {
        return "Application{"
                + "id=" + getId()
                + ", name='" + getName() + '\''
                + '}';
    }
}
