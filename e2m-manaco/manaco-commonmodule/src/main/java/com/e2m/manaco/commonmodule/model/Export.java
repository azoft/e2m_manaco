package com.e2m.manaco.commonmodule.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "export")
public class Export extends AbstractNameEntity {

    private static final String OWNER_COLUMN = "owner";
    private static final int OWNER_LENGTH = 255;
    private static final String STATUS_COLUMN = "status";
    private static final int STATUS_LENGTH = 50;
    private static final String TYPE_COLUMN = "type";
    private static final int TYPE_LENGTH = 50;
    private static final String URL_COLUMN = "url";
    private static final int URL_LENGTH = 1000;
    private static final String QUERY_COLUMN = "query";
    private static final int QUERY_LENGTH = 1000;

    private static final String EXPORT_RECORD_TABLE = "export_record";
    private static final String EXPORT_ID_COLUMN = "export_id";
    private static final String RECORD_ID_COLUMN = "record_id";

    @Column(name = OWNER_COLUMN, nullable = false, length = OWNER_LENGTH)
    private String owner;

    @Column(name = STATUS_COLUMN, nullable = false, length = STATUS_LENGTH)
    private String status;

    @Column(name = TYPE_COLUMN, nullable = false, length = TYPE_LENGTH)
    private String type;

    @Column(name = URL_COLUMN, length = URL_LENGTH)
    private String url;

    @Column(name = QUERY_COLUMN, length = QUERY_LENGTH)
    private String query;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = EXPORT_RECORD_TABLE, joinColumns = @JoinColumn(name = EXPORT_ID_COLUMN))
    @Column(name = RECORD_ID_COLUMN)
    @Fetch(FetchMode.SUBSELECT)
    private Set<Long> recordIds = new HashSet<>();

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Set<Long> getRecordIds() {
        return recordIds;
    }

    public void setRecordIds(Set<Long> recordIds) {
        this.recordIds = recordIds;
    }

    @Override
    public String toString() {
        return "Export{"
                + "id=" + getId()
                + ", name='" + getName() + '\''
                + ", owner='" + owner + '\''
                + ", status='" + status + '\''
                + ", type='" + type + '\''
                + ", url='" + url + '\''
                + ", query='" + query + '\''
                + ", recordIds=" + recordIds
                + '}';
    }
}
