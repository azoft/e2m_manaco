package com.e2m.manaco.commonmodule.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "server")
public class Server extends AbstractNameEntity {

    private static final String SERVER_GROUP_ID_COLUMN = "server_group_id";

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = SERVER_GROUP_ID_COLUMN)
    private ServerGroup group;

    public ServerGroup getGroup() {
        return group;
    }

    public void setGroup(ServerGroup group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return "Server{"
                + "id=" + getId()
                + ", name='" + getName() + '\''
                + ", group=" + group
                + '}';
    }
}
