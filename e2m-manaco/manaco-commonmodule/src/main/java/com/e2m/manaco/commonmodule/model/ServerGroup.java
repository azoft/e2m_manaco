package com.e2m.manaco.commonmodule.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "server_group")
public class ServerGroup extends AbstractNameEntity {

    @Override
    public String toString() {
        return "ServerGroup{"
                + "id=" + getId()
                + ", name='" + getName() + '\''
                + '}';
    }
}
