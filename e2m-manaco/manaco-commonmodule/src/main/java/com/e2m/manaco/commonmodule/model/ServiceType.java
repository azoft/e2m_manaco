package com.e2m.manaco.commonmodule.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "service_type")
public class ServiceType extends AbstractNameEntity {

    @Override
    public String toString() {
        return "ServiceType{"
                + "id=" + getId()
                + ", name='" + getName() + '\''
                + '}';
    }
}
