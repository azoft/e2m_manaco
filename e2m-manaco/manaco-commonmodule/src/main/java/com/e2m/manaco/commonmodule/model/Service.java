package com.e2m.manaco.commonmodule.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "service")
public class Service extends AbstractNameEntity {

    private static final String STATUS_COLUMN = "status";
    private static final int STATUS_LENGTH = 50;
    private static final String SERVICE_TYPE_ID_COLUMN = "service_type_id";
    private static final String SERVER_ID_COLUMN = "server_id";

    @Column(name = STATUS_COLUMN, nullable = false, length = STATUS_LENGTH)
    private String status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = SERVICE_TYPE_ID_COLUMN)
    private ServiceType type;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = SERVER_ID_COLUMN)
    private Server server;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ServiceType getType() {
        return type;
    }

    public void setType(ServiceType type) {
        this.type = type;
    }

    public Server getServer() {
        return server;
    }

    public void setServer(Server server) {
        this.server = server;
    }

    @Override
    public String toString() {
        return "Service{"
                + "id=" + getId()
                + ", name='" + getName() + '\''
                + ", status='" + status + '\''
                + ", type=" + type
                + ", server=" + server
                + '}';
    }
}
